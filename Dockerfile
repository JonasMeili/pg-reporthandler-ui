FROM mattrayner/lamp:latest-1804
COPY ./app ./app
COPY ./mysql ./mysql 
COPY ./tests ./tests 
COPY config.php .

CMD ["/run.sh"]

