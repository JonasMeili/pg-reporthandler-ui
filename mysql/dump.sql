-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: reporthandling
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `materialitem`
--

DROP TABLE IF EXISTS `materialitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `materialitem` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `LastUpdateTs` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `code` varchar(255) NOT NULL,
  `ItemDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materialitem`
--

LOCK TABLES `materialitem` WRITE;
/*!40000 ALTER TABLE `materialitem` DISABLE KEYS */;
INSERT INTO `materialitem` VALUES (1,'2020-07-28 11:25:12','1HC0031631R0005',NULL),(2,'2020-07-29 04:43:29','1HC0031628R0005',NULL),(3,'2020-07-29 04:44:21','1HC0028418V0010',NULL);
/*!40000 ALTER TABLE `materialitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaseorder`
--

DROP TABLE IF EXISTS `purchaseorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchaseorder` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `LastUpdateTs` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ExternalId` varchar(255) DEFAULT NULL,
  `Site` varchar(255) DEFAULT NULL,
  `AddressId` bigint DEFAULT NULL,
  `OrderDate` datetime DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `Tolerance` double DEFAULT NULL,
  `Status` int DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaseorder`
--

LOCK TABLES `purchaseorder` WRITE;
/*!40000 ALTER TABLE `purchaseorder` DISABLE KEYS */;
INSERT INTO `purchaseorder` VALUES (1,'2020-07-28 06:38:34','4500629284',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'2020-07-28 11:22:56','4500629286',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'2020-07-29 04:41:27','4500629286',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'2020-09-07 09:16:36','4548636263',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'2020-09-07 09:16:53','4548636263',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'2020-09-07 09:30:38','4548636263',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'2020-09-07 09:33:06','4548636863',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'2020-09-07 09:35:30','4548636863',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'2020-09-07 09:35:44','4548636263',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'2020-09-07 09:37:47','1048636863',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'2020-09-07 11:19:26','1048636863',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'2020-09-07 12:36:05','enternewpo',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'2020-09-08 17:42:29','123456789',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'2020-09-08 17:47:02','123456789',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,'2020-09-10 05:06:46','987654312',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'2020-09-10 05:10:42','987654312',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,'2020-09-10 05:11:09','987654312',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'2020-09-10 05:36:07','newPurchaseOrderNumber',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,'2020-09-10 05:57:44','123',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,'2020-09-10 11:14:45','#newPurchaseOrderNumber',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,'2020-09-10 12:00:06','2222',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,'2020-09-10 12:06:28','223333',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'2020-09-10 12:06:55','223333',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,'2020-09-10 12:14:35','223333',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,'2020-09-10 12:19:51','123123',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,'2020-09-10 12:20:42','44445353',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,'2020-09-10 12:33:42','8888',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,'2020-09-10 12:33:57','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,'2020-09-10 12:36:29','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,'2020-09-10 12:36:41','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,'2020-09-10 12:40:16','4566',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,'2020-09-10 12:40:40','4566',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,'2020-09-10 13:27:34','777767',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,'2020-09-10 13:52:21','9999',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `purchaseorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaseordergoodsreceived`
--

DROP TABLE IF EXISTS `purchaseordergoodsreceived`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchaseordergoodsreceived` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `LastUpdateTs` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `LotNumber` varchar(255) DEFAULT NULL,
  `PurchaseOrderLineId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `PurchaseOrderLineId` (`PurchaseOrderLineId`),
  CONSTRAINT `purchaseordergoodsreceived_ibfk_1` FOREIGN KEY (`PurchaseOrderLineId`) REFERENCES `purchaseorderline` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaseordergoodsreceived`
--

LOCK TABLES `purchaseordergoodsreceived` WRITE;
/*!40000 ALTER TABLE `purchaseordergoodsreceived` DISABLE KEYS */;
INSERT INTO `purchaseordergoodsreceived` VALUES (1,'2020-07-29 11:56:26','112233',1),(2,'2020-07-29 12:01:42','445566',2);
/*!40000 ALTER TABLE `purchaseordergoodsreceived` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaseordergoodsreceivedproperty`
--

DROP TABLE IF EXISTS `purchaseordergoodsreceivedproperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchaseordergoodsreceivedproperty` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `LastUpdateTs` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `GoodsReceivedId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `GoodsReceivedId` (`GoodsReceivedId`),
  CONSTRAINT `purchaseordergoodsreceivedproperty_ibfk_1` FOREIGN KEY (`GoodsReceivedId`) REFERENCES `purchaseordergoodsreceived` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaseordergoodsreceivedproperty`
--

LOCK TABLES `purchaseordergoodsreceivedproperty` WRITE;
/*!40000 ALTER TABLE `purchaseordergoodsreceivedproperty` DISABLE KEYS */;
INSERT INTO `purchaseordergoodsreceivedproperty` VALUES (1,'2020-07-30 03:45:16','Serialnumber','MIV20-16501128-16',1),(2,'2020-07-30 03:45:40','Serialnumber','MIV20-16501129-11',1),(3,'2020-07-30 03:45:51','Serialnumber','MIV20-16501131-19',1);
/*!40000 ALTER TABLE `purchaseordergoodsreceivedproperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaseorderline`
--

DROP TABLE IF EXISTS `purchaseorderline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchaseorderline` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `LastUpdateTs` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Line` int DEFAULT NULL,
  `Sequence` int DEFAULT NULL,
  `Quantity` double DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `MaterialItemId` bigint NOT NULL,
  `PurchaseOrderId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `MaterialItemId` (`MaterialItemId`),
  KEY `PurchaseOrderId` (`PurchaseOrderId`),
  CONSTRAINT `purchaseorderline_ibfk_1` FOREIGN KEY (`MaterialItemId`) REFERENCES `materialitem` (`Id`),
  CONSTRAINT `purchaseorderline_ibfk_2` FOREIGN KEY (`PurchaseOrderId`) REFERENCES `purchaseorder` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaseorderline`
--

LOCK TABLES `purchaseorderline` WRITE;
/*!40000 ALTER TABLE `purchaseorderline` DISABLE KEYS */;
INSERT INTO `purchaseorderline` VALUES (1,'2020-07-28 11:29:17',NULL,NULL,NULL,NULL,NULL,1,1),(2,'2020-07-28 12:15:34',NULL,NULL,NULL,NULL,NULL,1,2);
/*!40000 ALTER TABLE `purchaseorderline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaseorderlineproperty`
--

DROP TABLE IF EXISTS `purchaseorderlineproperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchaseorderlineproperty` (
  `Id` bigint NOT NULL AUTO_INCREMENT,
  `LastUpdateTs` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Name` varchar(255) DEFAULT NULL,
  `Value` varchar(255) DEFAULT NULL,
  `PurchaseOrderLineId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `PurchaseOrderLineId` (`PurchaseOrderLineId`),
  CONSTRAINT `purchaseorderlineproperty_ibfk_1` FOREIGN KEY (`PurchaseOrderLineId`) REFERENCES `purchaseorderline` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaseorderlineproperty`
--

LOCK TABLES `purchaseorderlineproperty` WRITE;
/*!40000 ALTER TABLE `purchaseorderlineproperty` DISABLE KEYS */;
INSERT INTO `purchaseorderlineproperty` VALUES (1,'2020-08-06 05:26:29','ReportUrl','http://xe-i-chpg401p.xe.abb.com:8001/sap/bc/bsp/sap/zdm_mesview/view_docfile.htm?sap-client=100&dokar=AND&doknr=GL_20120814&dokvr=A&doktl=D',1),(2,'2020-08-06 05:26:55','ReportUrl','http://xe-i-chpg401p.xe.abb.com:8001/sap/bc/bsp/sap/zdm_mesview/view_docfile.htm?sap-client=100&dokar=AND&doknr=1HDG111033A&dokvr=Q&doktl=EN',1),(3,'2020-08-06 05:27:13','ReportUrl','http://xe-i-chpg401p.xe.abb.com:8001/sap/bc/bsp/sap/zdm_mesview/view_docfile.htm?sap-client=100&dokar=AND&doknr=1HDG511150A&dokvr=F&doktl=EN',1),(4,'2020-08-06 05:27:34','ReportUrl','http://xe-i-chpg401p.xe.abb.com:8001/sap/bc/bsp/sap/zdm_mesview/view_docfile.htm?sap-client=100&dokar=NOR&doknr=GMS4770034P0002&dokvr=A&doktl=001',2),(8,'2020-08-06 06:12:47','ReportUrl','C:\\Users\\chjomei2\\Desktop\\Attachment\\Material_List_27K 6893 10.pdf',2),(9,'2020-08-06 06:13:20','ReportUrl','C:\\Users\\chjomei2\\Desktop\\Attachment\\Material_List_164 6790 20.pdf',2);
/*!40000 ALTER TABLE `purchaseorderlineproperty` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-14 22:38:30
