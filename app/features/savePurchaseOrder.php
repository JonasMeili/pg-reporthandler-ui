<?php
$configs = require_once("../../config.php");
require_once("../include/php helper/database.php");

$purchaseorder = $_POST['newPurchaseOrderNumber'];


if (is_numeric($purchaseorder) && (strlen($purchaseorder)>0) && (strlen($purchaseorder)<=10) ){        
    $db = new Database($configs);
    $db -> Query("select ExternalId from purchaseorder where purchaseorder.ExternalId = ($purchaseorder );");
    if (($db -> numRows())== 0){
        $db -> UpdateDb("INSERT INTO purchaseorder (ExternalId) VALUES ($purchaseorder);");
        $output = json_encode(array('type'=>'success', 'message' => 'Purchase Order ' .$purchaseorder. ' succesfully registered!'));
    }else {
        $output = json_encode(array('type'=>'error', 'message' => 'Purchase order already exists!'));
    }    
}
else {
    $output = json_encode(array('type'=>'error', 'message' => 'Wrong input format!'));
}
die($output);
?>
