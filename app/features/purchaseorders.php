<?php
require_once("../include/php helper/htmlHelper.php");
require_once("../include/php helper/database.php");
$configs = require_once("../../config.php");

$htmlHelper = new HtmlHelper();
$scripts = "<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>
<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>

<script src='../include/js/purchaseorders.js'></script>";


$header = $htmlHelper -> getHeader("ReportHandler",$scripts);
$navbar = $htmlHelper -> getNavbar("purchaseorders");

//HTML
echo $header;
echo $navbar;
echo "<div class='container'>";
echo "<br>";
echo showPurchaseOrderControls();
echo "<br><br>";
echo showInboxTable();
echo showModalManageComponents();

echo "</div></body></html>";


function showInboxTable (){
    $html = "<h2>Inbox infoQS</h2>
            <table id='InboxTable' class= 'display' style='width:100%'>
            <thead><tr><th>Subject</th><th>Filename</th><th>Document-URL</th></tr></thead>
            <tbody></tbody>
            </table>";

    return $html;
}


function showPurchaseOrderControls(){
    $html = "<div class id='messages'></div><br>";
    
    $html.= "<form><div class = 'form-row'><div class='col-6'>   
            <div class='input-group mb-3'>    
            <input type='text' name='inputpo' class='form-control' id='enternewpo'   placeholder='Register a new purchaseorder'>                               
            <div class='input-group-append'>
                <button type = 'button' class='btn btn-primary' disabled id='savePurchaseOrder'>SAVE</button>
            </div></div></div>";

    $html.= "<div class='col'>   
            <button type='button' class='btn btn-primary' disabled id='btnManageComponents'>MANAGE COMPONENTS</button>
            </div>";
            
    $html.= "</div></form>";

    return $html;
}

function showModalManageComponents(){
    $html = "<div class='modal fade' id='modalManageComponents' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
        <div class='modal-dialog modal-dialog-centered' role='document'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <h5 class='modal-title' id='ModalTitle'>Manage components of purchase order XY</h5>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                    </button>
                </div>
            <div class='modal-body'>
                <p>ToDo:Excel-Table with multiple components</p>                
                <input type='text'  class='form-control mb-2' id='enterSerNr'  style='width: 300px;' placeholder='New serialnumber'>                               
                <input type='text'  class='form-control mb-2' id='enterBatch'  style='width: 300px;' placeholder='New batchnumber'>
                <input type='text'  class='form-control mb-2' id='enterMatNr'  style='width: 300px;' placeholder='New materialnumber'>                              
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                <button type='button' class='btn btn-primary' id='btnSaveComponents' >Save changes</button>
            </div>
            </div>
        </div>
    </div>";
    return $html;

}