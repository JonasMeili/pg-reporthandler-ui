<?php
require_once("../include/php helper/htmlHelper.php");

$configs = require_once("../../config.php");

$htmlHelper = new HtmlHelper();
$scripts = 
"<script src='../include/js/traceability.js'></script>";

$header = $htmlHelper -> getHeader("ReportHandler",$scripts);
$navbar = $htmlHelper -> getNavbar("traceability");





//HTML ausgabe
echo $header;
echo $navbar;
echo "<div class='container'>";
echo "<br>";
echo showComponentTable();
echo "<br><br>";
echo showDocumentTable();
   

echo "</div></body></html>";

function showComponentTable(){
    $html = "<table id='componentTable' class= 'display' style='width:100%'>
            <thead><tr><th>Purchase Order</th><th>Serialnumber</th><th>Batch</th></tr></thead>
            <tbody></tbody>
            </table>";
    return $html;
}


function showDocumentTable(){
    $html = "<table id= 'documentTable' class = 'display>' style='width: 100%'>
            <thead><tr><th>Purchase Order</th>
            <th>Document</th></tr></thead>
            <tbody></tbody>                       
            </table>";
    return $html;
}