<?php

class HtmlHelper {	
	public static function getHeader($title, $scripts){
        $header = '<!DOCTYPE html>
        <html>
            <head>
                <meta charset="UTF-8">
                <title>'.$title.'</title>
                <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
                <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
                '.$scripts.'
            </head>
            <body>';
        return $header;
    }


    public static function getNavbar($active){
        global $configs;
        $curdir = getcwd();

        $navbar = "
                <nav class='navbar navbar-dark bg-dark'>
                <a class='navbar-brand'";
        if ($active == "index"){
            $navbar .= 
                " active ";
            }
        if (strpos($curdir, "features") !==false){
            $navbar .= "href='../index.php'>ReportHandler</a><nav class='nav'>                   
                    <a class='nav-link'";
        }
        else {
            $navbar .=  "href='./index.php'>ReportHandler</a><nav class='nav'>                   
            <a class='nav-link'";
        }

        if ($active == "purchaseorders") {
            $navbar .=
                " active ";
        }
        if (strpos($curdir, "features") !==false){
            $navbar .= "href='./purchaseorders.php'>Purchase Orders</a>     
            <a class='nav-link'"; 
            }
        else {
            $navbar .= "href='./features/purchaseorders.php'>Purchase Orders</a>                
            <a class='nav-link'"; 
        }

        
        if ($active == "traceability"){
            $navbar .= 
                " active ";
        }

        if (strpos($curdir, "features") !==false){
            $navbar .= "href='./traceability.php'>Traceability</a>";
        }
        else {
            $navbar .= "href='./features/traceability.php'>Traceability</a>";         
        }

        $navbar .= "<a class='nav-link disabled' href='#' tabindex='-1' aria-disabled='true'>Help</a>
        </nav></nav>";
        

        return  $navbar;
    }
    

    
    
}
?>