<?php

class Database {	

	protected $link;
	protected $result;
	protected $numRows;
		
	// Establish connection to database, when class is instantiated
	public function __construct(array $configs) {
		$this->link = new mysqli($configs["dbhost"], $configs["user"], $configs["password"], $configs["dbname"]);
		if(mysqli_connect_errno()) {
			echo "Connection Failed: " . mysqli_connect_errno();
			exit();
		}
	}
	
	// Sends the query to the connection
	public function Query($sql) {
		$this->result = $this->link->query($sql) or die(mysqli_error($this->result));
		$this->numRows = mysqli_num_rows($this->result);	
	}
	
	// Inserts into databse
	public function UpdateDb($sql) {
		$this->result = $this->link->query($sql) or die(mysqli_error($this->result));
		return $this->link;
	}
	
	// Return the number of rows
	public function NumRows() {
		return $this->numRows;		
	}

	// Return the last inserted ID
	public function lastId() {
		return $this->insert_id;		
	}
	
	// Fetchs the rows and return them
	public function Rows() {
		$rows = array();
		
		for($x = 0; $x < $this->NumRows(); $x++) {
			$rows[] = mysqli_fetch_assoc($this->result);
		}
		return $rows;
	}


	
	
	// Used by other classes to get the connection
	public function GetLink() {
		return $this->link;
	}
	
	// Securing input data
	public function SecureInput($value) {
		return mysqli_real_escape_string($this->link, $value);
	}
}
?>