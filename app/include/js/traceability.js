$(document).ready(function(){            
    $.get( "./getComponents.php", function( data ) {
        console.log(data);                    
        var componentArray = JSON.parse(data);
                    
        componentArray.forEach(function(component){   
            var body ='';
            body += '<tr>';
            body += '<td>' + component.ExternalId;
            body += '<td>' + component.Value;
            body += '<td>' + component.Lotnumber;
            $('#componentTable').append(body);                               
        });
        $('#componentTable').DataTable({
            paging: false
        });
    });
    
    $.get( "./getDocuments.php", function( data ) {
        console.log(data);                    
        var documentArray = JSON.parse(data);
                    
        documentArray.forEach(function(document){
            var body ='';
            body += '<tr>';
            body += '<td>' + document.ExternalId;
            body += '<td><a href=' + document.Value +'>'+document.Value+ '<\a>';                    
            $('#documentTable').append(body);                               
        });
        $('#documentTable').DataTable({
            paging: false
        });
    });
})