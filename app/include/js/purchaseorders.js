 $(document).ready(function(){
    //show documents:
    $.get("./getDocuments.php", function( data ) {
        console.log(data);                    
        var documentArray = JSON.parse(data);                    
        documentArray.forEach(function(document){
            var body ='';
            body += '<tr>';
            body += '<td>' + document.ExternalId;
            body += '<td>';
            body += '<td><a href=' + document.Value +'>'+document.Value+ '<\a>';                    
            $('#InboxTable').append(body);                               
        });
        $('#InboxTable').DataTable({
            'paging': false,
            'dom': 'lrti'                    
        });
    });


    //validate new purchase order number:
    $("#enternewpo").change(function(){        
    var newPurchaseOrderNumber = $('#enternewpo').val().trim();
            console.log(newPurchaseOrderNumber);            
            if (isNaN(newPurchaseOrderNumber)){            
                document.getElementById('savePurchaseOrder').disabled = true;      
                document.getElementById('btnManageComponents').disabled = true; 
                alerting("#messages","alert alert-warning","Only numbers allowed!");
            }
            else {
                document.getElementById('savePurchaseOrder').disabled = false;
                document.getElementById('btnManageComponents').disabled = false;
                $('#enternewpo').val(newPurchaseOrderNumber);
            }                  
    });
    
    //register a new purchase order
    $("#savePurchaseOrder").click(function(){
        var newPurchaseOrderNumber = $("#enternewpo").val();
        $.ajax({
            url: './savePurchaseOrder.php',
            method: 'post',
            dataType: 'json',
            data: {newPurchaseOrderNumber:newPurchaseOrderNumber},
            success: function (response) {
                if(response.type == 'success') {
                    alerting("#messages","alert alert-success",response.message);
                } else {
                    alerting("#messages","alert alert-danger",response.message);
                }
                console.log(response.message);  
            }
            
        });
                
    });


    //modal-dialog for managing components:
    $('#btnManageComponents').click(function(){
        var PurchaseOrderNumber = $("#enternewpo").val();
        $.ajax({
            url: './getIdOfPurchaseOrder.php',
            method: 'get',
            dataType: 'json',
            data: {PurchaseOrderNumber:PurchaseOrderNumber},
            success: function (response) {
                if(response.type == 'error') {     
                    alerting("#messages","alert alert-danger",response.message);
                    document.getElementById('btnManageComponents').disabled = true;                    
                    console.log(response.message);  
                } else {                    
                    var currentPoArray = response;
                    var PoId,PoNr;
                    currentPoArray.forEach(function(po){
                        PoId = po.Id;
                        PoNr = po.ExternalId;                        
                    });
                    document.getElementById("ModalTitle").innerHTML = ("Manage components of purchase order "+ PoNr);
                    $('#modalManageComponents').modal();
                }
                
            }
            
        });
        
      });
    

    //register new components --> Development in progress!
    $('#btnSaveComponents').click(function(){
        var newSerialNumber = $("#enterSerNr").val();
        var newBatchNumber = $("#enterBatch").val();
        var newMaterialNumber = $("#enterMatNr").val();
        console.log("sending values "+newSerialNumber+" "+newBatchNumber+" "+newMaterialNumber+"...");
        
        $.ajax({
            url: './saveComponents.php',
            method: 'post',
            dataType: 'json',
            data: {
                newSerialNumber:newSerialNumber,
                newBatchNumber:newBatchNumber,
                newMaterialNumber:newMaterialNumber},
            success: function (response){                 
                console.log(response.message); 
            }
        });

    });    
});



function alerting($id, $style, $text){
    $($id).addClass($style).text($text);    
    $($id).fadeTo(2000, 500).slideUp(1500, function(){
        $($id).slideUp(500);
        $($id).removeClass($style);
    });  
}

